<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "mywebsite";

// Conectați-vă la baza de date
$conn = mysqli_connect($host, $username, $password, $database);

if (!$conn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['modificaid'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $name = $_POST['name'];
    $address = $_POST['address'];
    $birthday = $_POST['birthday'];

    $sql = "UPDATE users_info SET username = '$username', email = '$email', password = '$password', name = '$name', address = '$address', birthday = '$birthday' WHERE id = $id";

    if (mysqli_query($conn, $sql)) {
        $response = array('success' => true, 'message' => 'Userul a fost modificat cu succes.');
    } else {
        $response = array('success' => false, 'message' => 'Eroare la modificare: ' . mysqli_error($conn));
    }
    
    // Întoarceți răspunsul sub formă de JSON
    header('Content-Type: application/json');
    echo json_encode($response);

    // Nu mai este necesară redirecționarea aici
}

mysqli_close($conn);
?>
