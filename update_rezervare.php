<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "rezervare";

// Conectați-vă la baza de date
$conn = mysqli_connect($host, $username, $password, $database);

if (!$conn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['modificaid'];
    $nume = $_POST['nume'];
    $email = $_POST['email'];
    $persoane = $_POST['persoane'];
    $hotel = $_POST['hotel'];
    $check_in = $_POST['check_in'];
    $check_out = $_POST['check_out'];

    $sql = "UPDATE rezervare SET nume = '$nume', email = '$email', persoane = '$persoane', hotel = '$hotel', check_in = '$check_in', check_out = '$check_out' WHERE id = $id";

    if (mysqli_query($conn, $sql)) {
        $response = array('success' => true, 'message' => 'Rezervarea a fost modificat cu succes.');
    } else {
        $response = array('success' => false, 'message' => 'Eroare la modificare: ' . mysqli_error($conn));
    }
    
    // Întoarceți răspunsul sub formă de JSON
    header('Content-Type: application/json');
    echo json_encode($response);

    // Nu mai este necesară redirecționarea aici
}

mysqli_close($conn);
?>
