<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "myhotel";

// Conectați-vă la baza de date
$conn = mysqli_connect($host, $username, $password, $database);

if (!$conn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['modificaid'];
    $nume = $_POST['nume'];
    $locatie = $_POST['locatie'];
    $site = $_POST['site'];
    $contact = $_POST['contact'];
    $rating = $_POST['rating'];

    $sql = "UPDATE hotels_info SET nume = '$nume', locatie = '$locatie', site = '$site', contact = '$contact', rating = '$rating' WHERE id = $id";

    if (mysqli_query($conn, $sql)) {
        $response = array('success' => true, 'message' => 'Hotelul a fost modificat cu succes.');
    } else {
        $response = array('success' => false, 'message' => 'Eroare la modificare: ' . mysqli_error($conn));
    }
    
    // Întoarceți răspunsul sub formă de JSON
    header('Content-Type: application/json');
    echo json_encode($response);

    // Nu mai este necesară redirecționarea aici
}

mysqli_close($conn);
?>
