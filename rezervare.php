<?php
// Database connection details
$host = "localhost"; // Schimbă cu hostname-ul serverului tău MySQL
$username = "root"; // Schimbă cu numele de utilizator MySQL
$password = ""; // Lasă parola goală pentru conexiuni fără parolă
$database = "rezervare"; // Schimbă cu numele bazei de date

// Crează o conexiune la baza de date
$connn = mysqli_connect($host, $username, $password, $database);

// Verifică conexiunea
if (!$connn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

// Răspunsul JSON inițial
$response = array('message' => '', 'success' => false);

// Verifică dacă formularul a fost trimis
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obține datele introduse de utilizator din formular
    $nume = mysqli_real_escape_string($connn, $_POST["nume"]);
    $email = mysqli_real_escape_string($connn, $_POST["email"]);
    $persoane = $_POST["persoane"];
    $hotel = mysqli_real_escape_string($connn, $_POST["hotel"]); 
    $check_in =$_POST["check_in"];
    $check_out =$_POST["check_out"];

    // Verifică dacă numele de utilizator sau adresa de email există deja
    $sql = "SELECT * FROM rezervare WHERE nume='$nume'";
    $result = mysqli_query($connn, $sql);

    if (!$result) {
        // Eroare la interogare
        $response['message'] = mysqli_error($connn);
    } else {
        if (mysqli_num_rows($result) > 0) {
            // Numele de utilizator sau adresa de email există deja
            $response['message'] = "Ai deja o rezervare";
        } else {
            // Inserează datele utilizatorului în baza de date
            $sql = "INSERT INTO rezervare (nume, email, persoane, hotel, check_in, check_out ) VALUES ('$nume', '$email', '$persoane', '$hotel', '$check_in', '$check_out')";

            if (mysqli_query($connn, $sql)) {
                // Înregistrare reușită
                $response['message'] = "Rezervare reușită..";
                $response['success'] = true;
            } else {
                // Eroare la inserare
                $response['message'] = mysqli_error($connn);
            }
        }
    }
}

// Închide conexiunea la baza de date
mysqli_close($connn);

// Returnează răspunsul ca JSON
header('Content-Type: application/json');
echo json_encode($response);
?>
