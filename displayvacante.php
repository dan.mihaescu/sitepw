
<!DOCTYPE html>
<html style="font-size: 16px;">
<head>
    <style>
    .table {
      width: 100%;
      border-collapse: collapse;
    }

    .table th, .table td {
      border: 2px solid black;
      padding: 8px;
      text-align: left;
    }

    .table tbody tr:nth-child(even) {
      background-color: #d3d3d3; /* Culorea pentru rândurile pare */
    } 

    .table tbody tr:hover {
      background-color: #ddd; /* Culorea de hover */
    }
  </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Contact Us">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Display Vacante</title>
    <link rel="stylesheet" href="nicepage.css" media="screen">
    <link rel="stylesheet" href="Register.css" media="screen">
    <script class="u-script" type="text/javascript" src="jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="nicepage.js" defer=""></script>
    <meta name="generator" content="Nicepage 3.8.0, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700">
    
    <script type="application/ld+json">{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": "",
		"url": "index.html"
    }</script>
    <meta property="og:title" content="Registration Form">
    <meta property="og:type" content="website">
    <meta name="theme-color" content="#478ac9">
    <link rel="canonical" href="index.html">
    <meta property="og:url" content="index.html">
  </head>
  <body class="u-body">
    <header class="u-clearfix u-header u-header" id="sec-a00e">
      <div class="u-clearfix u-sheet u-sheet-1">
        <nav class="u-menu u-menu-dropdown u-offcanvas u-menu-1">
          <div class="menu-collapse" style="font-size: 1rem; letter-spacing: 0px; font-weight: 500;">
            <a class="u-button-style u-custom-active-border-color u-custom-active-color u-custom-border u-custom-border-color u-custom-borders u-custom-hover-border-color u-custom-hover-color u-custom-left-right-menu-spacing u-custom-padding-bottom u-custom-text-active-color u-custom-text-color u-custom-text-hover-color u-custom-top-bottom-menu-spacing u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="#">
              <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#menu-hamburger"></use></svg>
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><symbol id="menu-hamburger" viewBox="0 0 16 16" style="width: 16px; height: 16px;"><rect y="1" width="16" height="2"></rect><rect y="7" width="16" height="2"></rect><rect y="13" width="16" height="2"></rect></symbol></defs></svg>
            </a>
          </div>
          <div class="u-custom-menu u-nav-container">
            <ul class="u-nav u-spacing-2 u-unstyled u-nav-1">
              <li class="u-nav-item"><a class="u-active-grey-10 u-button-style u-hover-grey-5 u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="Home.html" style="padding: 10px 20px;">Home</a></li>
              <li class="u-nav-item"><a class="u-active-grey-10 u-button-style u-hover-grey-5 u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="Europa.html" style="padding: 10px 20px;">Europa</a>
                <div class="u-nav-popup">
                  <ul class="u-h-spacing-20 u-nav u-unstyled u-v-spacing-10 u-nav-2">
                    <!-- Your Europa menu items here -->
                  </ul>
                </div>
              </li>
              <li class="u-nav-item"><a class="u-active-grey-10 u-button-style u-hover-grey-5 u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="America-Asia-sau-Africa.html" style="padding: 10px 20px;">America Asia sau Africa</a>
                <div class="u-nav-popup">
                  <ul class="u-h-spacing-20 u-nav u-unstyled u-v-spacing-10 u-nav-3">
                    <!-- Your America Asia sau Africa menu items here -->
                  </ul>
                </div>
              </li>
              <li class="u-nav-item"><a class="u-active-grey-10 u-button-style u-hover-grey-5 u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="Perioade-de-călătorit.html" style="padding: 10px 20px;">Perioade de călătorit</a>
                <div class="u-nav-popup">
                  <ul class="u-h-spacing-20 u-nav u-unstyled u-v-spacing-10 u-nav-4">
                    <!-- Your Perioade de călătorit menu items here -->
                  </ul>
                </div>
              </li>
              </li><li class="u-nav-item"><a class="u-active-grey-10 u-button-style u-hover-grey-5 u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="display.php" style="padding: 10px 20px;">Display</a><div class="u-nav-popup"><ul class="u-h-spacing-20 u-nav u-unstyled u-v-spacing-10 u-nav-4"><li class="u-nav-item"><a class="u-button-style u-nav-link u-text-body-color u-white" href="display.php" data-page-id="72483418">Display users</a>
                </li><li class="u-nav-item"><a class="u-button-style u-nav-link u-text-body-color u-white" href="displayabonat.php">Display abonati</a>
                </li><li class="u-nav-item"><a class="u-button-style u-nav-link u-text-body-color u-white" href="displayhotel.php" >Display hoteluri</a>
                </li><li class="u-nav-item"><a class="u-button-style u-nav-link u-text-body-color u-white" href="displayrezervare.php">Display rezervari</a>
                </li><li class="u-nav-item"><a class="u-button-style u-nav-link u-text-body-color u-white" href="displayvacante.php">Display vacante</a>
              </li></ul>
              
          </div>
          <div class="u-custom-menu u-nav-container-collapse">
            <div class="u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
              <div class="u-menu-close"></div>
              <ul class="u-align-center u-nav u-popup-spacing u-unstyled u-nav-5">
                <li class="u-nav-item"><a class="u-button-style u-hover-grey-5 u-nav-link u-white u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="Home.html" style="padding: 10px 20px;">Home</a></li>
                <li class="u-nav-item"><a class="u-button-style u-hover-grey-5 u-nav-link u-white u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="Europa.html" style="padding: 10px 20px;">Europa</a></li>
                <li class="u-nav-item"><a class="u-button-style u-hover-grey-5 u-nav-link u-white u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="America-Asia-sau-Africa.html" style="padding: 10px 20px;">America Asia sau Africa</a></li>
                <li class="u-nav-item"><a class="u-button-style u-hover-grey-5 u-nav-link u-white u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="Perioade-de-călătorit.html" style="padding: 10px 20px;">Perioade de călătorit</a></li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link u-white" href="Register.html" style="padding: 10px 20px;">Register</a></li>
              </ul>
            </div>
            <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
          </div>
        </nav>
      </div>
    </header>
    <section class="u-align-center u-clearfix u-grey-5 u-section-1" id="sec-b740" style="padding-bottom: 100px;">
      <div class="u-clearfix u-sheet u-sheet-1">
        <h1 class="u-custom-font u-font-roboto-slab u-text u-text-1">Display vacante</h1>
        <div>&nbsp;</div>
        <div class="u-align-left u-form-group u-form-adduser">
            <li class="u-nav-item"><a class="u-border-2 u-border-black u-active-grey-10 u-button-style u-hover-grey-5 u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="Vacante.html" style="padding: 10px 20px;">Adauga vacanta</a></li>  
            </div>
            <div class="u-h-spacing-20 ">
              <div>&nbsp;</div>
  <table class="u-border-2 u-border-black table">
  <thead>
    <tr style="background-color: #808080">
      <th class="u-border-2 u-border-black" scope="col">id</th>
      <th class="u-border-2 u-border-black" scope="col">destinatie</th>
      <th class="u-border-2 u-border-black" scope="col">oras de plecare</th>
      <th class="u-border-2 u-border-black" scope="col">data inceput</th>
      <th class="u-border-2 u-border-black" scope="col">data sfarsit</th>
      <th class="u-border-2 u-border-black" scope="col">transport</th>
      <th class="u-border-2 u-border-black" scope="col">pret</th>
      <th class="u-border-2 u-border-black" scope="col">modifica/sterge</th>
    </tr>
  </thead>
  <tbody>
  <?php
 
  $host = "localhost"; // Schimbă cu hostname-ul serverului tău MySQL
  $username = "root"; // Schimbă cu numele de utilizator MySQL
  $password = ""; // Lasă parola goală pentru conexiuni fără parolă
  $database = "vacante"; // Schimbă cu numele bazei de date
  
  // Crează o conexiune la baza de date
  $conn = mysqli_connect($host, $username, $password, $database);
  
  // Verifică conexiunea
  if (!$conn) {
      die("Conexiune eșuată: " . mysqli_connect_error());
  }
  

  $sql="Select * from `vacante`";
  $result=mysqli_query($conn,$sql);
  if($result){
    while($row=mysqli_fetch_assoc($result)){
        $id=$row['id'];
        $destinatie=$row['destinatie'];
        $oras_de_plecare=$row['oras_de_plecare'];
        $data_inceput=$row['data_inceput'];
        $data_sfarsit=$row['data_sfarsit'];
        $transport=$row['transport'];
        $pret=$row['pret'];
        echo '<tr>
        <th scope="row">'.$id.'</th>
        <td>'.$destinatie.'</td>
        <td>'.$oras_de_plecare.'</td>
        <td>'.$data_inceput.'</td>
        <td>'.$data_sfarsit.'</td>
        <td>'.$transport.'</td>
        <td>'.$pret.'</td>
        <td >
        <li class="u-nav-item"><a class="u-border-2 u-border-black u-active-grey-10 u-button-style u-hover-grey-5 u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="modifica_vacanta.php? modificaid='.$id.'" style="padding: 10px 20px;">Modifica</a></li>  
        <li class="u-nav-item"><a class="u-border-2 u-border-black u-active-grey-10 u-button-style u-hover-grey-5 u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="sterge_vacante.php? stergeid='.$id.'" style="padding: 10px 20px;">Sterge</a></li>  
        </td>
      </tr>';
    }
  }
  ?>
    <!-- <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td colspan="2">Larry the Bird</td>
      <td>@twitter</td>
    </tr> -->
  </tbody>
</table>
  </div>
      </div>
    </section>
  </body>

  <footer class="u-align-left u-clearfix u-footer u-grey-80 u-footer" id="sec-e867"><div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
    </div></footer>

</html>
