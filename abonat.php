<?php
// Database connection details
$host = "localhost"; // Schimbă cu hostname-ul serverului tău MySQL
$username = "root"; // Schimbă cu numele de utilizator MySQL
$password = ""; // Lasă parola goală pentru conexiuni fără parolă
$database = "abonat"; // Schimbă cu numele bazei de date

// Crează o conexiune la baza de date
$connn = mysqli_connect($host, $username, $password, $database);

// Verifică conexiunea
if (!$connn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

// Răspunsul JSON inițial
$response = array('message' => '', 'success' => false);

// Verifică dacă formularul a fost trimis
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obține datele introduse de utilizator din formular
    $email = mysqli_real_escape_string($connn, $_POST["email"]);

    // Verifică dacă numele de utilizator sau adresa de email există deja
    $sql = "SELECT * FROM abonat WHERE email='$email'";
    $result = mysqli_query($connn, $sql);

    if (!$result) {
        // Eroare la interogare
        $response['message'] = mysqli_error($connn);
    } else {
        if (mysqli_num_rows($result) > 0) {
            // Numele de utilizator sau adresa de email există deja
            $response['message'] = "Esti deja abonat.";
        } else {
            // Inserează datele utilizatorului în baza de date
            $sql = "INSERT INTO abonat (email) VALUES ('$email')";

            if (mysqli_query($connn, $sql)) {
                // Înregistrare reușită
                $response['message'] = "Abonare reușită..";
                $response['success'] = true;
            } else {
                // Eroare la inserare
                $response['message'] = mysqli_error($connn);
            }
        }
    }
}

// Închide conexiunea la baza de date
mysqli_close($connn);

// Returnează răspunsul ca JSON
header('Content-Type: application/json');
echo json_encode($response);
?>
