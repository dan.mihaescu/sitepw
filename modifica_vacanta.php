<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "vacante";

// Conectați-vă la baza de date
$conn = mysqli_connect($host, $username, $password, $database);

if (!$conn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

    $id = $_GET['modificaid'];
    $sql = "SELECT * FROM vacante WHERE id = $id";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    $destinatie=$row['destinatie'];
    $oras_de_plecare=$row['oras_de_plecare'];
    $data_inceput=$row['data_inceput'];
    $data_sfarsit=$row['data_sfarsit'];
    $transport=$row['transport'];
    $pret=$row['pret'];


mysqli_close($conn);
?>


<!DOCTYPE html>
<html style="font-size: 16px;">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Contact Us">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Vacanta Form</title>
    <link rel="stylesheet" href="nicepage.css" media="screen">
    <link rel="stylesheet" href="Hotel.css" media="screen">
    <script class="u-script" type="text/javascript" src="jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="nicepage.js" defer=""></script>
    <meta name="generator" content="Nicepage 3.8.0, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700">
    
    <script type="application/ld+json">{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": "",
		"url": "index.html"
    }</script>
    <meta property="og:title" content="Vacanta Form">
    <meta property="og:type" content="website">
    <meta name="theme-color" content="#478ac9">
    <link rel="canonical" href="index.html">
    <meta property="og:url" content="index.html">
  </head>
  <body class="u-body">
  <header class="u-clearfix u-header u-header u-grey-5 " id="sec-b740">
      <div class="u-clearfix u-sheet u-sheet-1">
        
      </div>
    </header>
    <section class="u-align-center u-clearfix u-grey-5 u-section-1" id="sec-b740">
      <div class="u-clearfix u-sheet u-sheet-1">
        <h1 class="u-custom-font u-font-roboto-slab u-text u-text-1">Update vacanta</h1>
        <div class="u-form u-form-1">
          <form action="update_vacanta.php" method="POST" class="u-clearfix u-form-spacing-16 u-form-vertical u-inner-form" style="padding: 2px;" source="custom" name="form">
          <div class="u-form-group u-form-name">
                <label for="destinatie" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-3">Destinatie</label>
                <input type="text" placeholder="Destinatie" id="destinatie" name="destinatie" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" autocomplete="off" value=<?php echo $destinatie; ?>>
              </div>
              <input type="hidden" name="modificaid" value="<?php echo $id; ?>">
            <div class="u-form-email u-form-group">
              <label for="oras_de_plecare" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-1">Oras de plecare</label>
              <input type="oras_de_plecare" placeholder="Oras de plecare" id="oras_de_plecare" name="oras_de_plecare" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="" autocomplete="off" value=<?php echo $oras_de_plecare; ?>>
            </div>
            <div class="u-form-date u-form-group u-form-group-4">
              <label for="data_inceput" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-4">Data de inceput</label>
              <input type="date" id="check_in" name="data_inceput" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" autocomplete="off" value=<?php echo $data_inceput; ?> >
            </div>
            <div class="u-form-date u-form-group u-form-group-4">
                <label for="data_sfarsit" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-4">Data de sfarsit</label>
                <input type="date" id="data_sfarsit" name="data_sfarsit" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" autocomplete="off" value=<?php echo $data_sfarsit; ?>>
              </div>
            <div class="u-form-email u-form-group">
                <label for="transport" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-1">Transport</label>
                <input type="transport" placeholder="Transport" id="transport" name="transport" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="" autocomplete="off" value=<?php echo $transport; ?>>
            </div>
            <div class="u-form-email u-form-group">
                <label for="pret" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-1">Pret</label>
                <input type="pret" placeholder="Pret" id="pret" name="pret" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="" autocomplete="off" value=<?php echo $pret; ?>>
            </div>
            <div class="u-align-center u-form-group u-form-submit">
              <input type="submit" value="Modifica vacanta" class="u-border-2 u-border-black u-btn u-btn-rectangle u-btn-submit u-button-style u-none u-btn-1">
            </div>
            <div id="registration-messages" style="color: transparent;">&nbsp;</div>
        </div>
      </div>
    </section>

    <!-- Adaugă acest script JavaScript în partea de jos a fișierului register.html -->
    <script>
    // Afișează mesajul de înregistrare în funcție de răspunsul JSON
    function displayRegistrationMessage(response) {
        var registrationMessages = document.getElementById("registration-messages");
        
        if (response.success) {
            registrationMessages.innerHTML = response.message;
            registrationMessages.style.color = "#00ff00";
            window.location.href = "displayvacante.php";
        } else {
            registrationMessages.innerHTML = response.message;
            registrationMessages.style.color = "#ff0000";
        }

        registrationMessages.style.display = "block";
    }

    // Trimite cererea POST către update.php la trimiterea formularului
    document.querySelector("form").addEventListener("submit", function (event) {
        event.preventDefault(); // Oprirea trimiterii formularului obișnuit
        var form = this;
        var data = new FormData(form);

        fetch('update_vacanta.php', {
            method: 'POST',
            body: data,
        })
        .then(response => response.json())
        .then(data => {
            displayRegistrationMessage(data);
        })
        .catch(error => {
            console.error('Eroare:', error);
        });
    });
</script>


  </body>
  <footer class="u-align-left u-clearfix u-footer u-grey-80 u-footer" id="sec-e867"><div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
  </div></footer>
</html>


