<?php
// Database connection details
$host = "localhost"; // Schimbă cu hostname-ul serverului tău MySQL
$username = "root"; // Schimbă cu numele de utilizator MySQL
$password = ""; // Lasă parola goală pentru conexiuni fără parolă
$database = "mywebsite"; // Schimbă cu numele bazei de date

// Crează o conexiune la baza de date
$conn = mysqli_connect($host, $username, $password, $database);

// Verifică conexiunea
if (!$conn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

// Răspunsul JSON inițial
$response = array('message' => '', 'success' => false);

// Verifică dacă formularul a fost trimis
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obține datele introduse de utilizator din formular
    $username = mysqli_real_escape_string($conn, $_POST["username"]);
    $email = mysqli_real_escape_string($conn, $_POST["email"]);
    $password = mysqli_real_escape_string($conn, $_POST["password"]);
    $name = mysqli_real_escape_string($conn, $_POST["name"]); 
    $address = mysqli_real_escape_string($conn, $_POST["address"]);
    $birthday =$_POST["date"];

    // Verifică dacă numele de utilizator sau adresa de email există deja
    $sql = "SELECT * FROM users_info WHERE username='$username' OR email='$email'";
    $result = mysqli_query($conn, $sql);

    if (!$result) {
        // Eroare la interogare
        $response['message'] = mysqli_error($conn);
    } else {
        if (mysqli_num_rows($result) > 0) {
            // Numele de utilizator sau adresa de email există deja
            $response['message'] = "Numele de utilizator sau adresa de email există deja. Te rugăm să alegi altele.";
        } else {
            // Inserează datele utilizatorului în baza de date
            $sql = "INSERT INTO users_info (username, email, password, name, address, birthday) VALUES ('$username', '$email', '$password', '$name', '$address', '$birthday')";

            if (mysqli_query($conn, $sql)) {
                // Înregistrare reușită
                $response['message'] = "Înregistrare reușită. Acum te poți autentifica.";
                $response['success'] = true;
            } else {
                // Eroare la inserare
                $response['message'] = mysqli_error($conn);
            }
        }
    }
}

// Închide conexiunea la baza de date
mysqli_close($conn);

// Returnează răspunsul ca JSON
header('Content-Type: application/json');
echo json_encode($response);
?>
