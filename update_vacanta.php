<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "vacante";

// Conectați-vă la baza de date
$conn = mysqli_connect($host, $username, $password, $database);

if (!$conn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['modificaid'];
    $destinatie = $_POST['destinatie'];
    $oras_de_plecare = $_POST['oras_de_plecare'];
    $data_inceput = $_POST['data_inceput'];
    $data_sfarsit = $_POST['data_sfarsit'];
    $transport = $_POST['transport'];
    $pret = $_POST['pret'];

    $sql = "UPDATE vacante SET destinatie = '$destinatie', oras_de_plecare = '$oras_de_plecare', data_inceput = '$data_inceput', data_sfarsit = '$data_sfarsit', transport = '$transport', pret = '$pret' WHERE id = $id";

    if (mysqli_query($conn, $sql)) {
        $response = array('success' => true, 'message' => 'Vacanta a fost modificat cu succes.');
    } else {
        $response = array('success' => false, 'message' => 'Eroare la modificare: ' . mysqli_error($conn));
    }
    
    // Întoarceți răspunsul sub formă de JSON
    header('Content-Type: application/json');
    echo json_encode($response);

    // Nu mai este necesară redirecționarea aici
}

mysqli_close($conn);
?>
