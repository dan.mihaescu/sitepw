<?php
session_start(); // Începe sau reia sesiunea

// Database connection details
$host = "localhost";
$username = "root";
$password = "";
$database = "mywebsite";

// Create a database connection
$conn = mysqli_connect($host, $username, $password, $database);

// Check the connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check if the form was submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Get user input from the form
    $username = mysqli_real_escape_string($conn, $_POST["username"]);
    $password = mysqli_real_escape_string($conn, $_POST["password"]);

    // Assume the authentication was unsuccessful by default
    $authentication_successful = false;

    // Query to check if the user exists in the database
    $sql = "SELECT * FROM users_info WHERE username='$username'";
    $result = mysqli_query($conn, $sql);

    if ($result) {
        if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            // Check the password
            if ($password == $row['password']) {
                $authentication_successful = true;
                $_SESSION['autentificat'] = true; // Setează variabila de sesiune
                $_SESSION['username'] = $username;
            }
        }
    }

    // Return a response based on authentication success
    if ($authentication_successful) {
        echo "success";
    } else {
        echo "failure";
    }
}

// Close the database connection
mysqli_close($conn);

?>