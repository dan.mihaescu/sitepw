<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "rezervare";

// Conectați-vă la baza de date
$conn = mysqli_connect($host, $username, $password, $database);

if (!$conn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

    $id = $_GET['modificaid'];
    $sql = "SELECT * FROM rezervare WHERE id = $id";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    $nume=$row['nume'];
    $email=$row['email'];
    $persoane=$row['persoane'];
    $hotel=$row['hotel'];
    $check_in=$row['check_in'];
    $check_out=$row['check_out'];


mysqli_close($conn);
?>


<!DOCTYPE html>
<html style="font-size: 16px;">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Contact Us">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Rezervare Form</title>
    <link rel="stylesheet" href="nicepage.css" media="screen">
    <link rel="stylesheet" href="Hotel.css" media="screen">
    <script class="u-script" type="text/javascript" src="jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="nicepage.js" defer=""></script>
    <meta name="generator" content="Nicepage 3.8.0, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700">
    
    <script type="application/ld+json">{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": "",
		"url": "index.html"
    }</script>
    <meta property="og:title" content="Rezervare Form">
    <meta property="og:type" content="website">
    <meta name="theme-color" content="#478ac9">
    <link rel="canonical" href="index.html">
    <meta property="og:url" content="index.html">
  </head>
  <body class="u-body">
    <header class="u-clearfix u-header u-header u-grey-5 " id="sec-b740">
      <div class="u-clearfix u-sheet u-sheet-1">
        
      </div>
    </header>
    <section class="u-align-center u-clearfix u-grey-5 u-section-1" id="sec-b740">
      <div class="u-clearfix u-sheet u-sheet-1">
        <h1 class="u-custom-font u-font-roboto-slab u-text u-text-1">Update rezervare</h1>
        <div class="u-form u-form-1">
          <form action="update_rezervare.php" method="POST" class="u-clearfix u-form-spacing-16 u-form-vertical u-inner-form" style="padding: 2px;" source="custom" name="form">
          <div class="u-form-group u-form-name">
                <label for="nume" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-3">Nume</label>
                <input type="text" placeholder="Nume" id="nume" name="nume" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" autocomplete="off" value = <?php echo $nume; ?>>
              </div>
            <div class="u-form-email u-form-group">
              <label for="email" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-1">Email</label>
              <input type="email" placeholder="Email" id="email" name="email" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="" autocomplete="off" value = <?php echo $email; ?>>
            </div>
            <input type="hidden" name="modificaid" value="<?php echo $id; ?>">
            <div class="u-form-email u-form-group">
                <label for="persoane" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-1">Persoane</label>
                <input type="persoane" placeholder="Persoane" id="persoane" name="persoane" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="" autocomplete="off" value = <?php echo $persoane; ?>>
            </div>
            <div class="u-form-email u-form-group">
                <label for="hotel" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-1">Hotel</label>
                <input type="hotel" placeholder="Hotel" id="hotel" name="hotel" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="" autocomplete="off" value = <?php echo $hotel; ?>>
            </div>
            <div class="u-form-date u-form-group u-form-group-4">
              <label for="check_in" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-4">Check-in</label>
              <input type="date" id="check_in" name="check_in" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" autocomplete="off" value = <?php echo $check_in; ?>>
            </div>
            <div class="u-form-date u-form-group u-form-group-4">
                <label for="check_out" class="u-custom-font u-heading-font u-label u-text-palette-5-dark-3 u-label-4">Check-out</label>
                <input type="date" id="check_out" name="check_out" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" autocomplete="off" value = <?php echo $check_out; ?>>
              </div>
            <div class="u-align-center u-form-group u-form-submit">
              <input type="submit" value="Update Rezervare" class="u-border-2 u-border-black u-btn u-btn-rectangle u-btn-submit u-button-style u-none u-btn-1">
            </div>
            <div id="registration-messages" style="color: transparent;">&nbsp;</div>
          </form>
        </div>
      </div>
    </section>

    <!-- Adaugă acest script JavaScript în partea de jos a fișierului register.html -->
    <script>
    // Afișează mesajul de înregistrare în funcție de răspunsul JSON
    function displayRegistrationMessage(response) {
        var registrationMessages = document.getElementById("registration-messages");
        
        if (response.success) {
            registrationMessages.innerHTML = response.message;
            registrationMessages.style.color = "#00ff00";
            window.location.href = "displayrezervare.php";
        } else {
            registrationMessages.innerHTML = response.message;
            registrationMessages.style.color = "#ff0000";
        }

        registrationMessages.style.display = "block";
    }

    // Trimite cererea POST către update.php la trimiterea formularului
    document.querySelector("form").addEventListener("submit", function (event) {
        event.preventDefault(); // Oprirea trimiterii formularului obișnuit
        var form = this;
        var data = new FormData(form);

        fetch('update_rezervare.php', {
            method: 'POST',
            body: data,
        })
        .then(response => response.json())
        .then(data => {
            displayRegistrationMessage(data);
        })
        .catch(error => {
            console.error('Eroare:', error);
        });
    });
</script>


  </body>
  <footer class="u-align-left u-clearfix u-footer u-grey-80 u-footer" id="sec-e867"><div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
  </div></footer>
</html>


