<?php
// Database connection details
$host = "localhost"; // Schimbă cu hostname-ul serverului tău MySQL
$username = "root"; // Schimbă cu numele de utilizator MySQL
$password = ""; // Lasă parola goală pentru conexiuni fără parolă
$database = "vacante"; // Schimbă cu numele bazei de date

// Crează o conexiune la baza de date
$connn = mysqli_connect($host, $username, $password, $database);

// Verifică conexiunea
if (!$connn) {
    die("Conexiune eșuată: " . mysqli_connect_error());
}

// Răspunsul JSON inițial
$response = array('message' => '', 'success' => false);

// Verifică dacă formularul a fost trimis
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obține datele introduse de utilizator din formular
    $destinatie = mysqli_real_escape_string($connn, $_POST["destinatie"]);
    $oras_de_plecare = mysqli_real_escape_string($connn, $_POST["oras_de_plecare"]);
    $data_inceput =$_POST["data_inceput"];
    $data_sfarsit =$_POST["data_sfarsit"];
    $transport = mysqli_real_escape_string($connn, $_POST["transport"]);
    $pret = $_POST["pret"];
    // Verifică dacă numele de utilizator sau adresa de email există deja
    $sql = "SELECT * FROM vacante WHERE (destinatie='$destinatie' AND oras_de_plecare='$oras_de_plecare')";
    $result = mysqli_query($connn, $sql);

    if (!$result) {
        // Eroare la interogare
        $response['message'] = mysqli_error($connn);
    } else {
        if (mysqli_num_rows($result) > 0) {
            // Numele de utilizator sau adresa de email există deja
            $response['message'] = "A fost adaugata acea vacanta";
        } else {
            // Inserează datele utilizatorului în baza de date
            $sql = "INSERT INTO vacante (destinatie, oras_de_plecare, data_inceput, data_sfarsit, transport, pret ) VALUES ('$destinatie', '$oras_de_plecare', '$data_inceput', '$data_sfarsit', '$transport', '$pret')";

            if (mysqli_query($connn, $sql)) {
                // Înregistrare reușită
                $response['message'] = "Vacanta adaugata ..";
                $response['success'] = true;
            } else {
                // Eroare la inserare
                $response['message'] = mysqli_error($connn);
            }
        }
    }
}

// Închide conexiunea la baza de date
mysqli_close($connn);

// Returnează răspunsul ca JSON
header('Content-Type: application/json');
echo json_encode($response);
?>
